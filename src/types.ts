export enum EDocumentDescription {
  INDEX_PAGE = "",
  EXTRA = "extra",
  NOT_FOUND_PAGE = "Not Found",
}

export enum EIconLibrary {
  BRANDS = "fab",
  MATERIAL = "material",
  CUSTOM = "custom",
}
